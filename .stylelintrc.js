module.exports = {
    extends: [
        "stylelint-config-standard",
        "stylelint-config-rational-order"
    ],
    plugins: [
        "stylelint-order",
        "stylelint-scss"
    ],
    rules: {
        "declaration-empty-line-before": null,
        "plugin/rational-order": [
            true,
            {
                "border-in-box-model": false,
                "empty-line-between-groups": true
            }
        ]
    }
};
