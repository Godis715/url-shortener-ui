import { Result } from "types/ResultWrapper";

export interface IUrlShortenerService {
    shortenLink(url: string): Promise<Result<string>>;
}
