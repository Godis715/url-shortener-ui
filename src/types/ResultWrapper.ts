export class SuccessResult<T> {
    readonly error = undefined;

    readonly value: T;

    constructor(value: T) {
        this.value = value;
    }
}

export class ErrorResult {
    readonly error: Error;

    constructor(error: Error) {
        this.error = error;
    }
}

export type Result<T = undefined> = SuccessResult<T> | ErrorResult;
