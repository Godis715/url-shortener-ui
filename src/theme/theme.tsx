import "./style.scss";

const theme = {
    button: {
        wrapper: "Button"
    },
    input: {
        inputWrapper: "Input"
    }
};

export default theme;
