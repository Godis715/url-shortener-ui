import React, { useState } from "react";
import { cn } from "@bem-react/classname";
import * as L from "korus-ui";
import "./ResultPanel.scss";
import PageSection from "component/PageSection/PageSection";
import Button from "component/Button/Button";
import { ReactComponent as ArrowIcon } from "./arrow.svg";

type Props = {
    isResultNew: boolean;
    inputUrl: string;
    shortUrl: string;
};

const cnResultPanel = cn("ResultPanel");

function ResultPanel(props: Props): JSX.Element {
    const {
        isResultNew,
        inputUrl,
        shortUrl
    } = props;

    const [isCopied, setIsCopied] = useState(false);

    const handleCopy = () => {
        navigator.clipboard
            .writeText(shortUrl)
            .then(() => {
                setIsCopied(true);
                setTimeout(() => setIsCopied(false), 2000);
            })
            .catch((err) => {
                // eslint-disable-next-line no-console
                console.warn("Не удалось скопировать текст", err);
            });
    };

    const title = isResultNew
        ? "Готово! Вы можете скопировать ссылку ниже"
        : "Предыдущая сокращенная ссылка";

    return (
        <PageSection
            title={title}
            className={cnResultPanel()}
        >
            <L.Div className={cnResultPanel("Content")}>
                <L.Input placeholder="http://example-url" value={inputUrl} />
                <L.Div className={cnResultPanel("Arrow")}>
                    <ArrowIcon />
                </L.Div>
                <L.Input placeholder="http://example-url" value={shortUrl} />
                <Button onClick={handleCopy} isDisabled={isCopied}>
                    {isCopied
                        ? "Скопировано!"
                        : "Скопировать"}
                </Button>
            </L.Div>
        </PageSection>
    );
}

export default ResultPanel;
