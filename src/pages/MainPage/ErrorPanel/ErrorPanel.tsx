import React from "react";
import { cn } from "@bem-react/classname";
import * as L from "korus-ui";
import PageSection from "component/PageSection/PageSection";
import "./ErrorPanel.scss";

type Props = {
    errorMessage: string;
};

const cnErrorPanel = cn("ErrorPanel");

function ErrorPanel(props: Props): JSX.Element {
    const {
        errorMessage
    } = props;

    return (
        <PageSection
            title="Что-то пошло не так..."
            className={cnErrorPanel()}
        >
            <L.Div className={cnErrorPanel("ErrorContainer")}>
                {errorMessage}
            </L.Div>
        </PageSection>
    );
}

export default ErrorPanel;
