import React, { useReducer } from "react";
import { cn } from "@bem-react/classname";
import * as L from "korus-ui";
import PageLayout from "layouts/PageLayout";
import urlShortenerService from "services/urlShortenerService";
import URLShortenerPanel from "./URLShortenerPanel/URLShortenerPanel";
import ResultPanel from "./ResultPanel/ResultPanel";
import RulesPanel from "./RulesPanel/RulesPanel";
import ErrorPanel from "./ErrorPanel/ErrorPanel";
import { FORM_NAME, URL_INPUT_NAME } from "./formConfig";
import "./MainPage.scss";

enum ActionType {
    ClearError,
    StartWaitingResult,
    HandleResult,
    HandleError,
    MarkResultAsOld
}

type ShortenerResult = {
    inputUrl: string;
    shortUrl: string;
};

type State = {
    error: Error | null;
    result: ShortenerResult | null;
    isWaitingResult: boolean;
    isResultNew: boolean;
};

type Action = {
    type: ActionType.ClearError;
} | {
    type: ActionType.StartWaitingResult;
} | {
    type: ActionType.HandleResult;
    result: ShortenerResult;
} | {
    type: ActionType.HandleError;
    error: Error;
} | {
    type: ActionType.MarkResultAsOld;
};

function reducer(state: State, action: Action): State {
    switch (action.type) {
        case ActionType.ClearError: {
            return {
                ...state,
                error: null
            };
        }
        case ActionType.StartWaitingResult: {
            return {
                ...state,
                isWaitingResult: true
            };
        }
        case ActionType.HandleError: {
            return {
                ...state,
                isWaitingResult: false,
                error: action.error,
                result: null
            };
        }
        case ActionType.HandleResult: {
            return {
                ...state,
                isWaitingResult: false,
                result: action.result,
                error: null,
                isResultNew: true
            };
        }
        case ActionType.MarkResultAsOld: {
            return {
                ...state,
                isResultNew: false
            };
        }
        default: {
            return state;
        }
    }
}

function resetUrlShortenerForm(): void {
    L.form(FORM_NAME, URL_INPUT_NAME).reset();
}

const cnMainPage = cn("MainPage");

function MainPage(): JSX.Element {
    const [state, dispatch] = useReducer(reducer, {
        error: null,
        result: null,
        isWaitingResult: false,
        isResultNew: true
    });

    const handleSubmit = async (url: string) => {
        dispatch({ type: ActionType.StartWaitingResult });
        const result = await urlShortenerService.shortenLink(url);

        if (result.error) {
            dispatch({
                type: ActionType.HandleError,
                error: result.error
            });
        }
        else {
            resetUrlShortenerForm();
            dispatch({
                type: ActionType.HandleResult,
                result: {
                    inputUrl: url,
                    shortUrl: result.value
                }
            });
        }
    };

    const markResultAsOld = () => {
        dispatch({ type: ActionType.MarkResultAsOld });
    };

    return (
        <PageLayout className={cnMainPage()}>
            <L.H1 className={cnMainPage("Title")}>
                Сервис сокращения ссылок
            </L.H1>
            <L.Div className={cnMainPage("Content")}>
                <URLShortenerPanel
                    isLoading={state.isWaitingResult}
                    onSubmit={handleSubmit}
                    onFocusUrlInput={markResultAsOld}
                />
                {state.error && (
                    <ErrorPanel
                        errorMessage={state.error.message}
                    />
                )}
                {state.result && (
                    <ResultPanel
                        isResultNew={state.isResultNew}
                        shortUrl={state.result.shortUrl}
                        inputUrl={state.result.inputUrl}
                    />
                )}
                <RulesPanel />
            </L.Div>
        </PageLayout>
    );
}

export default MainPage;
