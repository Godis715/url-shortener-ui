import React from "react";
import Button from "component/Button/Button";
import PageSection from "component/PageSection/PageSection";
import * as L from "korus-ui";
import { cn } from "@bem-react/classname";
import { FORM_NAME, URL_INPUT_NAME } from "../formConfig";
import "./URLShortenerPanel.scss";

type Props = {
    isLoading: boolean;
    onFocusUrlInput: () => void;
    onSubmit: (url: string) => void;
};

const cnPanel = cn("URLShortenerPanel");

function URLShortenerPanel(props: Props): JSX.Element {
    const {
        isLoading,
        onSubmit,
        onFocusUrlInput
    } = props;

    const handleSubmit = (ev: L.ButtonTypes.SubmitEvent) => {
        const url = ev.form?.[FORM_NAME]?.[URL_INPUT_NAME]?.value || "";
        onSubmit(url);
    };

    return (
        <PageSection
            title="Вставьте ссылку для сокращения"
            className={cnPanel()}
        >
            <L.Div className={cnPanel("Content")}>
                <L.Input
                    isRequired
                    form={FORM_NAME}
                    name={URL_INPUT_NAME}
                    validator="url"
                    placeholder="http://example-url.com"
                    className={cnPanel("InputCont")}
                    onFocus={onFocusUrlInput}
                />
                <Button
                    form={FORM_NAME}
                    onClick={handleSubmit}
                    isLoading={isLoading}
                >
                    Сократить
                </Button>
            </L.Div>
        </PageSection>
    );
}

export default URLShortenerPanel;
