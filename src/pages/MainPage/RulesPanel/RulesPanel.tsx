import React from "react";
import { cn } from "@bem-react/classname";
import * as L from "korus-ui";
import PageSection from "component/PageSection/PageSection";
import "./RulesPanel.scss";

const cnRulesPanel = cn("RulesPanel");

function RulesPanel(): JSX.Element {
    return (
        <PageSection title="Правила использования сервиса" className={cnRulesPanel()}>
            <L.Ol className={cnRulesPanel("List")}>
                <L.Li className={cnRulesPanel("ListItem")}>
                    Запрещено сокращать ссылки на вредоносные сайты
                </L.Li>
                <L.Li className={cnRulesPanel("ListItem")}>
                    Запрещено сокращать уже сокращенные ссылки
                </L.Li>
            </L.Ol>
        </PageSection>
    );
}

export default RulesPanel;
