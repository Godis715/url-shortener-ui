import React from "react";
import ReactDOM from "react-dom";
import { Leda } from "korus-ui";
import App from "./App/App";
import theme from "./theme/theme";
import "./index.scss";

ReactDOM.render(
    <React.StrictMode>
        <Leda theme={theme}>
            <App />
        </Leda>
    </React.StrictMode>,
    document.getElementById("root")
);
