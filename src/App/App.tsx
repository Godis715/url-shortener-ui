import React, { useState } from "react";
import * as L from "korus-ui";
import "./App.scss";
import MainPage from "pages/MainPage/MainPage";
import ThemeToggler from "component/ThemeToggler/ThemeToggler";
import ColorTheme, { ColorThemeType } from "../component/ColorTheme/ColorTheme";

function App(): JSX.Element {
    const [theme, setTheme] = useState(ColorThemeType.Light);

    const handleToggleTheme = () => {
        setTheme(
            theme === ColorThemeType.Light
                ? ColorThemeType.Dark
                : ColorThemeType.Light
        );
    };

    return (
        <ColorTheme themeType={theme}>
            <L.Div _App>
                <MainPage />
                <L.Div _App-ThemeTogglerCont>
                    <ThemeToggler
                        toggleTheme={handleToggleTheme}
                        activeTheme={theme}
                    />
                </L.Div>
            </L.Div>
        </ColorTheme>
    );
}

export default App;
