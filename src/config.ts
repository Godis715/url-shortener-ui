/**
 * Т.к. из этого файла может экспортироваться и одно переменная
 * (но это никак не default экспорт)
 */
/* eslint-disable import/prefer-default-export */

if (!process.env.REACT_APP_API_URL) {
    throw new Error("REACT_APP_API_URL must be provided as env variable");
}

export const { REACT_APP_API_URL } = process.env;
