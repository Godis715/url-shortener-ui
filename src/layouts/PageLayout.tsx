import React from "react";
import { cn } from "@bem-react/classname";
import "./PageLayout.scss";

type Props = {
    children?: React.ReactNode;
    className?: string;
};

const cnLayout = cn("PageLayout");

function PageLayout(props: Props): JSX.Element {
    const { children, className } = props;
    return (
        <div className={cnLayout(null, [className])}>
            {children}
        </div>
    );
}

export default PageLayout;
