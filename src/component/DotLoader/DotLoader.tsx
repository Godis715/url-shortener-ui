import React from "react";
import { cn } from "@bem-react/classname";
import "./DotLoader.scss";

const cnDotLoader = cn("DotLoader");

function DotLoader(): JSX.Element {
    return (
        <div className={cnDotLoader()}>
            <div className={cnDotLoader("Dot")} />
        </div>
    );
}

export default DotLoader;
