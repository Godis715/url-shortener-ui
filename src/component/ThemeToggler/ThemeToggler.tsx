import React from "react";
import * as L from "korus-ui";
import { ColorThemeType } from "component/ColorTheme/ColorTheme";
import { ReactComponent as SunIcon } from "./sunIcon.svg";
import { ReactComponent as MoonIcon } from "./moonIcon.svg";
import "./ThemeToggler.scss";

type Props = {
    activeTheme: ColorThemeType;
    toggleTheme: () => void;
};

function ThemeToggler(props: Props): JSX.Element {
    const {
        activeTheme,
        toggleTheme
    } = props;

    return (
        <L.Button onClick={toggleTheme} _ThemeToggler>
            {activeTheme === ColorThemeType.Light
                ? <MoonIcon />
                : <SunIcon />}
        </L.Button>
    );
}

export default ThemeToggler;
