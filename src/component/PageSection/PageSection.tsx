import React from "react";
import * as L from "korus-ui";
import "./PageSection.scss";
import { cn } from "@bem-react/classname";

type Props = {
    title: string;
    children?: React.ReactNode;
    className?: string;
};

const cnSection = cn("PageSection");

function PageSection(props: Props): JSX.Element {
    const {
        title,
        children,
        className
    } = props;

    return (
        <L.Div className={cnSection(null, [className])}>
            <L.H2>{title}</L.H2>
            <L.Div className={cnSection("Content")}>
                {children}
            </L.Div>
        </L.Div>
    );
}

export default PageSection;
