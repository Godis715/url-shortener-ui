import React from "react";
import * as L from "korus-ui";
import DotLoader from "../DotLoader/DotLoader";

function Button(props: L.ButtonTypes.ButtonProps): JSX.Element {
    const { isLoading, children } = props;

    return (
        <L.Button {...props}>
            {children}
            {isLoading && <DotLoader />}
        </L.Button>
    );
}

export default Button;
