import React from "react";
import { cn } from "@bem-react/classname";
import "./ColorTheme.scss";

export enum ColorThemeType {
    Light = "light",
    Dark = "dark"
}

const mapThemeTypeToModif = {
    [ColorThemeType.Light]: "light",
    [ColorThemeType.Dark]: "dark"
};

type Props = {
    themeType: ColorThemeType;
    children?: React.ReactNode;
};

const cnColorTheme = cn("ColorTheme");

function ColorTheme(props: Props): JSX.Element {
    const {
        themeType,
        children
    } = props;

    const themeModif = mapThemeTypeToModif[themeType];

    return (
        <div className={cnColorTheme({ color: themeModif })}>
            {children}
        </div>
    );
}

export default ColorTheme;
