import { REACT_APP_API_URL } from "config";
import { ErrorResult, Result, SuccessResult } from "types/ResultWrapper";
import { IUrlShortenerService } from "../types/IUrlShortenerService";

function isValidBody(body: unknown): body is { shortLink: string } {
    if (typeof body !== "object" || !body) {
        return false;
    }

    return "shortLink" in body;
}

class ShortLinkDemon implements IUrlShortenerService {
    async shortenLink(url: string): Promise<Result<string>> {
        try {
            const response = await fetch(`${REACT_APP_API_URL}/shortlink`, {
                method: "POST",
                body: JSON.stringify({ link: url }),
                headers: {
                    "Content-Type": "application/json"
                }
            });

            if (response.status === 429) {
                return new ErrorResult(
                    new Error("Сервер перегружен. Попробуйте позже")
                );
            }

            if (response.status !== 200) {
                return new ErrorResult(
                    new Error("Сервер вернул непредвиденную ошибку.")
                );
            }

            const body = await response.json();

            if (isValidBody(body)) {
                return new SuccessResult(body.shortLink);
            }

            return new ErrorResult(
                new Error("Сервер вернул недействительный ответ")
            );
        }
        catch (err) {
            return new ErrorResult(
                new Error("Произошла непредвиденная ошибка")
            );
        }
    }
}

export default ShortLinkDemon;
